const path = require('path');

module.exports = {
    webpack: function (config, env) {

        // add plugin
        if (!config.plugins) {
            config.plugins = [];
        }
        config.resolve = {
            ...config.resolve,
            alias: {
                ...config.resolve.alias,
                store: path.resolve(__dirname, 'src/store'),
                types: path.resolve(__dirname, 'src/types'),
                utility: path.resolve(__dirname, 'src/utility'),
            },
        };
        return config;
    },
    jest: function (config) {
        // ...add your jest config customisation...
        return config;
    },
    devServer: function (configFunction) {
        return function (proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);

            config.proxy = {
                ...config.proxy,
                '/api': {
                    target: 'https://eacp.energyaustralia.com.au',
                    secure: true,
                    logLevel: 'debug',
                    changeOrigin: true,
                    pathRewrite: {
                        '^/api': '/codingtest/api'
                    },
                }
            }
            return config;
        };
    },
    paths: function (paths, env) {
        return paths;
    },
}