export interface AppStore {
  isRequesting: boolean;
  error: boolean;
  recordLabels: Array<RecordLabelForDisplay>;
  bands: Array<BankForDisplay>;
}

export interface Band {
  name: string;
  recordLabel: string;
}

export interface MusicFestivalPayload {
  name: string;
  bands: Array<Band>;
}

export interface BankForDisplay {
  name: string;
  festivals: Array<string>;
}

export interface RecordLabelForDisplay {
  name: string;
  banks: Array<string>;
}
