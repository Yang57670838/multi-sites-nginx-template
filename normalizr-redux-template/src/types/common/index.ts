export interface ApiResponse {
    status: number;
}

export interface ErrorException {
    code: string;
    response: {
        status?: number;
        data?: any;
    };
}
