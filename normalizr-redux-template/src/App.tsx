import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getIdV4 } from "utility/common";
import { RootState } from "./store";
import { appActions } from "./store/slice/app";

import styles from "./App.module.css";

function App() {
  const dispatch = useDispatch();
  const { app } = useSelector((state: RootState) => state);
  const [errMsg, setErrMsg] = useState<string>("");

  useEffect(() => {
    dispatch(appActions.requestStartAction());
  }, [dispatch]);

  useEffect(() => {
    if (!app.isRequesting) {
      if (app.error) {
        setErrMsg("Error happens");
      } else {
        setErrMsg("");
      }
    }
  }, [app.isRequesting]);

  if (app.isRequesting) return <div>isLoading...</div>;
  return <>
  <div>{errMsg}</div>
  {app.recordLabels.map(r => (
    <div key={getIdV4()}>
      <h2>{r.name}</h2>
      {r.banks.map(band => (
        <div key={getIdV4()}>
          <div className={styles.bandTitle}>{band}</div>
          <div>
            {app.bands.find(b => b.name === band)?.festivals.map(f => (
              <div key={getIdV4()}>{f}</div>
            ))}
            </div>
        </div>
      ))}
    </div>
  ))}
  </>;
}

export default App;
