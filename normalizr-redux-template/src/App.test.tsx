import React from 'react';
import { render, screen } from '@testing-library/react';
import renderer from "react-test-renderer";
import * as reactRedux from "react-redux";
import App from './App';

jest.mock("react-redux", () => (
  {
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn(),
    useDispatch: jest.fn(),
  }
))

describe("<App />", () => {
  const useDispatchMock = jest.fn();
  const originalUseState = React.useState;
  const setErrMsgMock = jest.fn();

  beforeEach(() => {
    (reactRedux.useDispatch as any).mockImplementation(() => useDispatchMock);
    (reactRedux.useSelector as any).mockImplementation((callback: any) => {
      return callback({
        app: {
          isRequesting: false,
          error: false,
          recordLabels: [],
          bands: [],
        }
      })
    });
  })

  afterEach(() => {
    jest.resetAllMocks();
    React.useState = originalUseState;
  })

  it("init app by dispatch fetch start action", () => {
    render(<App />);
    expect(useDispatchMock).toBeCalledTimes(1);
    expect(useDispatchMock.mock.calls[0][0].type).toBe("app/requestStartAction")
  });

  it("display error if fetch failed", () => {
    (reactRedux.useSelector as any).mockImplementation((callback: any) => {
      return callback({
        app: {
          isRequesting: false,
          error: true,
          recordLabels: [],
          bands: [],
        }
      })
    });
    React.useState = jest.fn().mockReturnValue(["", setErrMsgMock])
    render(<App />);
    expect(setErrMsgMock).toBeCalledTimes(1);
    expect(setErrMsgMock.mock.calls[0][0]).toBe("Error happens")
  });

  it("display isLoading... if is fetching", () => {
    (reactRedux.useSelector as any).mockImplementation((callback: any) => {
      return callback({
        app: {
          isRequesting: true,
          error: false,
          recordLabels: [],
          bands: [],
        }
      })
    });
    render(<App />);
    expect(screen.getByText("isLoading...")).toBeInTheDocument();
  });

  it("display fetched content correctly", () => {
    (reactRedux.useSelector as any).mockImplementation((callback: any) => {
      return callback({
        app: {
          isRequesting: false,
          error: false,
          recordLabels: [
            {
              name: "Anti Records",
              banks: ["YOUKRANE"],
            },
          ],
          bands: [
            {
              name: "YOUKRANE",
              festivals: ["Trainerella"],
            },
          ],
        }
      })
    });
    React.useState = jest.fn().mockReturnValue(["", setErrMsgMock])
    render(<App />);
    expect(setErrMsgMock).toBeCalledTimes(1);
    expect(setErrMsgMock.mock.calls[0][0]).toBe("")
    expect(screen.getByText("Anti Records")).toBeInTheDocument();
    expect(screen.getByText("YOUKRANE")).toBeInTheDocument();
    expect(screen.getByText("Trainerella")).toBeInTheDocument();
  });

  test("snapshot", () => {
    (reactRedux.useSelector as any).mockImplementation((callback: any) => {
      return callback({
        app: {
          isRequesting: false,
          error: false,
          recordLabels: [
            {
              name: "Anti Records",
              banks: ["YOUKRANE"],
            },
          ],
          bands: [
            {
              name: "YOUKRANE",
              festivals: ["Trainerella"],
            },
          ],
        }
      })
    });
    const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });
})
