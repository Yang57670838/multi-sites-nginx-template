import { v4 as uuidv4 } from "uuid";
import { ErrorException } from "../../types/common";

export const isErrorException = (x: any): x is ErrorException => {
    return typeof x.response?.status === "number" || typeof x.code === "string";
};

export const getIdV4 = (): string => {
  return uuidv4();
};
