import { getIdV4, isErrorException } from "./";

describe("common func", () => {
  test("getIdV4 func works correctly", () => {
    expect(getIdV4().length).toEqual(36);
  });

  test("isErrorException func works correctly", () => {
    const correctErrorMock = {
      code: 400,
      response: {
        status: 400,
      },
    };
    const incorrectErrorMock = {
        response: {
            data: 1
        }
    }
    expect(isErrorException(correctErrorMock)).toEqual(true);
    expect(isErrorException(incorrectErrorMock)).toEqual(false);
  });
});
