import { generateAPI } from "../index";

const getFestival = () => {
    return generateAPI().get("/api/v1/festivals");
};

export default getFestival;
