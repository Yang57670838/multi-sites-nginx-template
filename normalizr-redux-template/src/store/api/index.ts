import axios, { AxiosError, AxiosInstance } from "axios";

export const generateAPI = (baseUrl = "/"): AxiosInstance => {
    const api = axios.create({
        baseURL: baseUrl,
    });

    api.interceptors.response.use(
        (response) => {
            return response;
        },
        (error: AxiosError) => {
            const responseCode = error?.response?.status;
            switch (responseCode) {
                default:
                    console.error(responseCode);
            }
            return Promise.reject(error);
        }
    );
    return api;
};
