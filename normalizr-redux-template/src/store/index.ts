import { configureStore, combineReducers, AnyAction } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import createSagaMiddleware from "redux-saga";
import rootSaga from "./saga";
import appReducer from "./slice/app";

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

if (process.env.NODE_ENV === "development") {
  const { createLogger } = require("redux-logger"); // eslint-disable-line global-require
  const logger = createLogger({ duration: true });
  middlewares.push(logger);
}

const combinedReducer = combineReducers({
  app: appReducer,
});

const rootReducer = (state: any, action: AnyAction) => {
  return combinedReducer(state, action);
};

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(middlewares),
  devTools: process.env.NODE_ENV !== "production",
});

sagaMiddleware.run(rootSaga);

setupListeners(store.dispatch);

export type RootState = ReturnType<typeof store.getState>;

export default store;
