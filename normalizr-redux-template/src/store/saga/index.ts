import { fork, all } from "redux-saga/effects";
import userWatch from "./app";

export default function* rootSaga() {
    yield all([fork(userWatch)]);
}
