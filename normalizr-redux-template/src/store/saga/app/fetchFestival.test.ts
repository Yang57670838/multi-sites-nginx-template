import { testSaga, expectSaga } from "redux-saga-test-plan";
import * as matcher from "redux-saga-test-plan/matchers";
import { throwError } from "redux-saga-test-plan/providers";
import { appActions } from "../../slice/app";
import getFestival from "../../api/app/getFestival";
import responsePayload from "mock/respnse.json";
import fetchFestival from "./fetchFestival";

describe("fetchFestival saga", () => {
  test("works correctly", () => {
    testSaga(fetchFestival)
      .next()
      .call(getFestival)
      .next({
        data: responsePayload,
      })
      .put(appActions.requestSuccessAction(responsePayload))
      .next()
      .isDone();
  });

  test("works correctly when receives err response", () => {
    const error = {
        response: {
            data: {
                code: "anything",
                errors: []
            },
            status: 400
        }
    }
    return expectSaga(fetchFestival)
      .provide([[matcher.call.fn(getFestival), throwError(error as any)]])
      .put(appActions.requestFailedAction())
      .run();
  });
});
