import { call, put } from "redux-saga/effects";
import { appActions } from "store/slice/app";
import { ApiResponse } from "types/common";
import { MusicFestivalPayload } from "types/app";
import { isErrorException } from "utility/common";
import getFestival from "../../api/app/getFestival";

export interface FetchFestivalResponse extends ApiResponse {
    data: Array<MusicFestivalPayload>;
}

export default function* fetchFestival() {
    try {
        const payload: FetchFestivalResponse = yield call(getFestival);
        yield put(appActions.requestSuccessAction(payload.data));
    } catch (e) {
        if (isErrorException(e)) {
            yield put(appActions.requestFailedAction());
        }
    }
}
