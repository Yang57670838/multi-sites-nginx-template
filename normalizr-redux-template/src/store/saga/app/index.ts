import { takeLatest } from "redux-saga/effects";
import { appActions } from "../../slice/app";
import fetchFestival from "./fetchFestival";

export default function* watcherSaga() {
    yield takeLatest(appActions.requestStartAction.toString(), fetchFestival);
}
