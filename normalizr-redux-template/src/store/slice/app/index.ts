import { createSlice } from "@reduxjs/toolkit";
import { AppStore } from "types/app";
import { fetchSuccessHandler } from "./handlers/fetchSuccessHandler";

const initialState: AppStore = {
  isRequesting: false,
  error: false,
  recordLabels: [],
  bands: [],
};

const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    requestStartAction(state) {
      state.isRequesting = true;
    },
    requestFailedAction(state) {
      state.isRequesting = false;
      state.error = true;
    },
    requestSuccessAction(state, action) {
      state.isRequesting = false;
      state.error = false;
      const result = fetchSuccessHandler(action.payload);
      state.bands = result.bankList;
      state.recordLabels = result.recordLabelList;
    },
  },
});

export const appActions = appSlice.actions;

export default appSlice.reducer;
