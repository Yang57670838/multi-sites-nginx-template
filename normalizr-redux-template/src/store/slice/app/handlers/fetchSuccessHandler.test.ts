import { fetchSuccessHandler } from "./fetchSuccessHandler";
import responsePayload from "mock/respnse.json";

test("fetchSuccessHandler works correctly", () => {
  const payload1 = [
    {
      name: "Twisted Tour",
      bands: [
        { name: "Summon", recordLabel: "Outerscope" },
        { name: "Auditones", recordLabel: "Marner Sis. Recording" },
        { name: "Squint-281" },
      ],
    },
    {
      name: "random festival",
      bands: [{ name: "Summon2", recordLabel: "Outerscope" }],
    },
  ];
  const { recordLabelList, bankList } = fetchSuccessHandler(payload1 as any);
  expect(recordLabelList.length).toEqual(2);
  expect(recordLabelList.map(one => {
      return one.name
  })).toEqual(["Marner Sis. Recording", "Outerscope"]);
  expect(bankList.length).toEqual(4);
});
