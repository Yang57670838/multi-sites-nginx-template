import { MusicFestivalPayload, BankForDisplay, RecordLabelForDisplay } from "types/app";

export const fetchSuccessHandler = (payload: Array<MusicFestivalPayload>): {
    recordLabelList: Array<RecordLabelForDisplay>,
    bankList: Array<BankForDisplay>,
} => {
  const recordLabelList: Array<RecordLabelForDisplay> = [];
  const bankList: Array<BankForDisplay> = [];
  for (let f = 0; f < payload.length; f++) {
    const currentFestival = payload[f].name;
    for (let b = 0; b < payload[f].bands.length; b++) {
        // structure BankForDisplay
        const index = bankList.findIndex(oneBank => oneBank.name === payload[f].bands[b].name);
        if (index < 0) {
            const newBank: BankForDisplay = {
                name: payload[f].bands[b].name,
                festivals: [currentFestival],
            }
            bankList.push(newBank);
        } else {
            bankList[index].festivals.push(currentFestival);
        }
        // structure RecordLabelForDisplay
        if (payload[f].bands[b].recordLabel) {
            const recordLabelIndex = recordLabelList.findIndex(oneRecord => oneRecord.name === payload[f].bands[b].recordLabel);
            if (recordLabelIndex < 0 ) {
                const newRecordLabel: RecordLabelForDisplay = {
                    name: payload[f].bands[b].recordLabel,
                    banks: [payload[f].bands[b].name],
                }
                recordLabelList.push(newRecordLabel);
            } else {
                recordLabelList[recordLabelIndex].banks.push(payload[f].bands[b].name);
            }
        }
    }
  }
  // sorting
  recordLabelList.sort((a, b) => a.name.localeCompare(b.name));
  for (let r = 0; r < recordLabelList.length; r++) {
      // only sorting if more than one record
      if (recordLabelList[r].banks.length > 1) {
        recordLabelList[r].banks.sort((a, b) => a.localeCompare(b));
      }
  }
  for (let ind = 0; ind < bankList.length; ind++) {
    // only sorting if more than one record
    if (bankList[ind].festivals.length > 1) {
        bankList[ind].festivals.sort((a, b) => a.localeCompare(b));
      } 
  }
  return {
    recordLabelList,
    bankList,
  };
};
