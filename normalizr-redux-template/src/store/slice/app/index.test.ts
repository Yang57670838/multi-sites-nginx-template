import { configureStore } from "@reduxjs/toolkit";
import appSlice, { appActions } from "./";
import responsePayload from "mock/respnse.json";

jest.mock("./handlers/fetchSuccessHandler", () => ({
  fetchSuccessHandler: () => {
    return {
      recordLabelList: [
        {
          name: "Anti Records",
          banks: ["YOUKRANE"],
        },
      ],
      bankList: [
        {
          name: "YOUKRANE",
          festivals: ["Trainerella"],
        },
      ],
    };
  },
}));

describe("appSlice", () => {
  const store = configureStore({ reducer: appSlice });
  let state;

  test("should return the initial state", () => {
    state = store.getState();
    expect(state).toEqual({
      isRequesting: false,
      error: false,
      recordLabels: [],
      bands: [],
    });
  });

  test("should handle requestStartAction", () => {
    store.dispatch(appActions.requestStartAction());
    state = store.getState();
    expect(state.isRequesting).toEqual(true);
  });

  test("should handle requestFailedAction", () => {
    store.dispatch(appActions.requestFailedAction());
    state = store.getState();
    expect(state.isRequesting).toEqual(false);
    expect(state.error).toEqual(true);
  });

  test("should handle requestSuccessAction", () => {
    store.dispatch(appActions.requestSuccessAction(responsePayload));
    state = store.getState();
    expect(state.isRequesting).toEqual(false);
    expect(state.error).toEqual(false);
    expect(state.bands).toEqual([
      {
        name: "YOUKRANE",
        festivals: ["Trainerella"],
      },
    ]);
    expect(state.recordLabels).toEqual([
      {
        name: "Anti Records",
        banks: ["YOUKRANE"],
      },
    ]);
  });
});
