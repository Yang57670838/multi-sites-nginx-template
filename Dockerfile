FROM node:16-alpine AS staticbuilder1
WORKDIR /normalizrproject
COPY /normalizr-redux-template/package*.json ./
RUN npm install
COPY /normalizr-redux-template/. .
RUN npm run build

# start nginx
FROM nginx:alpine
EXPOSE 80

ARG BUILD_DATE

LABEL maintainer="yangliu1010my@gmail.com"
LABEL author="yang"
LABEL version="beta1.0"
LABEL name="multi-sites-nginx-template"
LABEL description="a nginx template to hold multiple virtual sites"
LABEL build_date=$BUILD_DATE

COPY nginx.conf /etc/nginx/nginx.conf
# RUN rm -rf ./*

# configure for normalizr-redux-template
RUN mkdir -p /usr/share/nginx/html/normalizr-redux-template
COPY nginx/normalizr-redux-template.conf /etc/nginx/config.d/
WORKDIR /usr/share/nginx/html/normalizr-redux-template
COPY --from=staticbuilder1 /normalizrproject/build .

# configure for project
RUN mkdir -p /usr/share/nginx/html/project
COPY nginx/project.conf /etc/nginx/config.d/
WORKDIR /usr/share/nginx/html/project
# TODO: COPY --from=staticbuilder1 /normalizrproject/build .

# permissions
RUN chown -R 1001:1001 /usr/share/nginx/html && chmod -R 755 /usr/share/nginx/html && \
    chown -R 1001:1001 /etc/nginx && chmod -R 755 /etc/nginx && \
    chown -R 1001:1001 /var/cache && \
    chown -R 1001:1001 /var/log/nginx && \
    chown -R 1001:1001 /etc/nginx/nginx.conf
RUN chmod -R 777 /var/run
USER 1001

#run
ENTRYPOINT ["nginx", "-g", "daemon off;"]

