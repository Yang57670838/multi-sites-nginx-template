## local run
### run
cd normalizr-redux-template <br />
npm start <br />
visit http://localhost:3000/ <br />

## local run with
### run
docker-compose up --build -d <br />
visit http://localhost:3050/ <br />

### stop
docker-compose down -v <br />

## test
cd normalizr-redux-template <br />
npm run test:coverage <br />
